export const HOST_URL = "https://my-json-server.typicode.com";

export const endpoints = {
  REST_APIs: {
    Product: {
      GetProduct: `${HOST_URL}/jubs16/Products/Products`
    }
  }
};
